# E-mail Ink

> An e-mail signature generator

[![Licence][licence-badge]][licence-url]
[![Release][release-badge]][release-url]

A simple e-mail signature generator which uses [sass](sass-url) and supports
multiple, testable projects. It also uses my [Brands](brands-url) project for
social media colours and links.

## Installation

:bulb: Requires [Node.js][node-url].

Run this code to clone and install the project:

```sh
git clone https://gitlab.org/carolinealing/email-ink.git
cd email-ink && npm install
```

## Usage

This project uses [EJS](https://ejs.co/) to merge contents of the `context.json`
file into the signature. Various social media links can be added by typing, for
example, `<%= links.gitlab %>` for a link to GitLab.

Following command creates a skeleton located in the `build` directory. Replace
`project-name` with the name of your choice. Default is `default`:

```
npm create [project-name]
```

The argument `-p`, means *production*. If it's present gulp builds a compressed
file ready for insertion into your e-mails, and if it's omitted gulp will watch
for code changes and update the browser accordingly.

```
npm start [project-name] [-p]
```

## Licence

Released under GNU-3.0. See [LICENSE][licence-url] for more.

Coded with :heart: by [carolinealing][user-url].

[brands-url]: https://gitlab.com/carolinealing/brands
[licence-badge]: https://img.shields.io/gitlab/license/carolinealing/email-ink.svg
[licence-url]: LICENSE
[node-url]: https://nodejs.org/en/download/
[preprocess-url]: https://www.npmjs.com/package/preprocess
[release-badge]: https://badgen.net/gitlab/release/carolinealing/email-ink.svg
[release-url]: https://gitlab.com/carolinealing/email-ink/-/releases
[sass-url]: http://sass-lang.com/
[user-url]: https://gitlab.com/carolinealing
